﻿using UnityEngine;
using System.Collections.Generic;

public static class ShapeBehaviorPool<T> where T : ShapeBehavior, new()
{
    static Stack<T> stack = new Stack<T>();

    public static T Get()
    {
        if (stack.Count > 0)
        {
            T behavior = stack.Pop();
#if UNITY_EDITOR
            behavior.IsReclaimed = false;
#endif
            return behavior;
        }
        return ScriptableObject.CreateInstance<T>();
    }

    public static void Reclaim(T behavior)
    {
#if UNITY_EDITOR
        behavior.IsReclaimed = true;
#endif
        stack.Push(behavior);
    }
}
