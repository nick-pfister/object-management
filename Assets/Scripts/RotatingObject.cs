﻿using UnityEngine;

public class RotatingObject : GameLevelObject
{
    [SerializeField]
    Vector3 angularVelocity = Vector3.zero;

    public override void GameUpdate()
    {
        transform.Rotate(angularVelocity * Time.deltaTime);
    }

    public override void Save(GameDataWriter writer)
    {
        writer.Write(transform.rotation);
    }

    public override void Load(GameDataReader reader)
    {
        Quaternion rotation = reader.ReadQuaternion();
        transform.rotation = rotation;
    }
}